package com.example.ivitruk.vkadditionalclient;

import android.app.Application;

import com.example.ivitruk.vkadditionalclient.di.component.AppComponent;
import com.example.ivitruk.vkadditionalclient.di.component.DaggerAppComponent;
import com.example.ivitruk.vkadditionalclient.di.module.AppModule;
import com.example.ivitruk.vkadditionalclient.view.activity.BaseActivity;

public final class App extends Application {

    private static App app;

    private static BaseActivity currentActivity;

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        initDaggerComponents();
    }

    private void initDaggerComponents() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return app.appComponent;
    }

    public static BaseActivity getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(BaseActivity activity) {
        currentActivity = activity;
    }
}
