package com.example.ivitruk.vkadditionalclient;

import android.support.v7.preference.NumberPickerPreference;
import android.support.v7.preference.NumberPickerPreferenceDialogFragmentCompat;
import android.view.View;

public class DrawerItem {

    private String title;

    private View.OnClickListener onClickListener;

    public DrawerItem(String title, View.OnClickListener onClickListener) {
        this.title = title;
        this.onClickListener = onClickListener;
    }

    public String getTitle() {
        return title;
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }
}
