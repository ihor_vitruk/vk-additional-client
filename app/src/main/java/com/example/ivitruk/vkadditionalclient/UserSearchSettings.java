package com.example.ivitruk.vkadditionalclient;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

public class UserSearchSettings {

    private final int sort;

    private final int sex;

    private final int status;

    private final int ageFrom;

    private final int ageTo;

    private final int birthDay;

    private final int birthMonth;

    private final int online;

    private final int hasPhoto;

    private final int canWritePrivateMessage;

    private final int canSendFriendRequest;

    private final int blacklisted;

    private final int blacklistedByMe;

    private final int friendStatus;

    public UserSearchSettings(int sort, int sex, int status, int ageFrom, int ageTo,
                              int birthDay, int birthMonth, int online, int hasPhoto,
                              int canWritePrivateMessage, int canSendFriendRequest,
                              int blacklisted, int blacklistedByMe, int friendStatus) {
        this.sort = sort;
        this.sex = sex;
        this.status = status;
        this.ageFrom = ageFrom;
        this.ageTo = ageTo;
        this.birthDay = birthDay;
        this.birthMonth = birthMonth;
        this.online = online;
        this.hasPhoto = hasPhoto;
        this.canWritePrivateMessage = canWritePrivateMessage;
        this.canSendFriendRequest = canSendFriendRequest;
        this.blacklisted = blacklisted;
        this.blacklistedByMe = blacklistedByMe;
        this.friendStatus = friendStatus;
    }

    public int getSort() {
        return sort;
    }

    public int getSex() {
        return sex;
    }

    public int getStatus() {
        return status;
    }

    public int getAgeFrom() {
        return ageFrom;
    }

    public int getAgeTo() {
        return ageTo;
    }

    public int getBirthDay() {
        return birthDay;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public int getOnline() {
        return online;
    }

    public int getHasPhoto() {
        return hasPhoto;
    }

    public int getCanWritePrivateMessage() {
        return canWritePrivateMessage;
    }

    public int getCanSendFriendRequest() {
        return canSendFriendRequest;
    }

    public int getBlacklisted() {
        return blacklisted;
    }

    public int getBlacklistedByMe() {
        return blacklistedByMe;
    }

    public int getFriendStatus() {
        return friendStatus;
    }

    public static UserSearchSettings read(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        UserSearchSettings settings = new UserSearchSettings(
                Integer.valueOf(prefs.getString(context.getString(R.string.sort_type_key),
                        context.getString(R.string.sort_type_default_value))),
                Integer.valueOf(prefs.getString(context.getString(R.string.sex_type_key),
                        context.getString(R.string.sex_type_default_value))),
                Integer.valueOf(prefs.getString(context.getString(R.string.status_type_key),
                        context.getString(R.string.status_type_default_value))),
                prefs.getInt(context.getString(R.string.age_from_key),
                        Integer.valueOf(context.getString(R.string.age_min_value))),
                prefs.getInt(context.getString(R.string.age_to_key),
                        Integer.valueOf(context.getString(R.string.age_min_value))),
                prefs.getInt(context.getString(R.string.birth_day_key),
                        Integer.valueOf(context.getString(R.string.birth_day_min_value))),
                Integer.valueOf(prefs.getString(context.getString(R.string.birth_month_key),
                        context.getString(R.string.birth_month_default_value))),
                prefs.getBoolean(context.getString(R.string.online_key), false) ? 1 : 0,
                prefs.getBoolean(context.getString(R.string.has_photo_key), false) ? 1 : 0,
                prefs.getBoolean(context.getString(R.string.can_write_key), false) ? 1 : 0,
                prefs.getBoolean(context.getString(R.string.can_add_friend_key), false) ? 1 : 0,
                Integer.valueOf(prefs.getString(context.getString(R.string.blacklisted_key),
                        context.getString(R.string.blacklisted_default_value))),
                Integer.valueOf(prefs.getString(context.getString(R.string.blacklisted_by_me_key),
                context.getString(R.string.blacklisted_default_value))),
                Integer.valueOf(prefs.getString(context.getString(R.string.friend_status_key),
                        context.getString(R.string.friend_status_default_value)))
        );
        return settings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserSearchSettings settings = (UserSearchSettings) o;

        if (sort != settings.sort) return false;
        if (sex != settings.sex) return false;
        if (status != settings.status) return false;
        if (ageFrom != settings.ageFrom) return false;
        if (ageTo != settings.ageTo) return false;
        if (birthDay != settings.birthDay) return false;
        if (birthMonth != settings.birthMonth) return false;
        if (online != settings.online) return false;
        if (hasPhoto != settings.hasPhoto) return false;
        if (canWritePrivateMessage != settings.canWritePrivateMessage) return false;
        if (canSendFriendRequest != settings.canSendFriendRequest) return false;
        if (blacklisted != settings.blacklisted) return false;
        if (blacklistedByMe != settings.blacklistedByMe) return false;
        return friendStatus == settings.friendStatus;

    }

    @Override
    public int hashCode() {
        int result = sort;
        result = 31 * result + sex;
        result = 31 * result + status;
        result = 31 * result + ageFrom;
        result = 31 * result + ageTo;
        result = 31 * result + birthDay;
        result = 31 * result + birthMonth;
        result = 31 * result + online;
        result = 31 * result + hasPhoto;
        result = 31 * result + canWritePrivateMessage;
        result = 31 * result + canSendFriendRequest;
        result = 31 * result + blacklisted;
        result = 31 * result + blacklistedByMe;
        result = 31 * result + friendStatus;
        return result;
    }
}
