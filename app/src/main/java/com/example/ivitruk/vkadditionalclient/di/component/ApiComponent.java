package com.example.ivitruk.vkadditionalclient.di.component;

import com.example.ivitruk.vkadditionalclient.di.module.ApiModule;
import com.example.ivitruk.vkadditionalclient.di.module.BusinessModule;
import com.example.ivitruk.vkadditionalclient.di.scope.BusinessScope;

import dagger.Component;
import dagger.Subcomponent;

@BusinessScope
@Subcomponent(modules = ApiModule.class)
public interface ApiComponent {
    BusinessComponent plus(BusinessModule businessModule);
}
