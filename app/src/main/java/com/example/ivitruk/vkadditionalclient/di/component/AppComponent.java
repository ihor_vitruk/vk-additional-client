package com.example.ivitruk.vkadditionalclient.di.component;

import com.example.ivitruk.vkadditionalclient.di.module.ApiModule;
import com.example.ivitruk.vkadditionalclient.di.module.AppModule;
import com.example.ivitruk.vkadditionalclient.di.scope.ApplicationScope;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@ApplicationScope
@Component(modules = AppModule.class)
public interface AppComponent {
    ApiComponent plus(ApiModule apiModule);
}
