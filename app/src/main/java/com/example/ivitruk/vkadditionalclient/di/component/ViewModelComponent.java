package com.example.ivitruk.vkadditionalclient.di.component;

import com.example.ivitruk.vkadditionalclient.di.module.ViewModelModule;
import com.example.ivitruk.vkadditionalclient.di.scope.ViewScope;
import com.example.ivitruk.vkadditionalclient.view.activity.LoginActivity;
import com.example.ivitruk.vkadditionalclient.view.activity.MainActivity;
import com.example.ivitruk.vkadditionalclient.view.activity.SplashActivity;
import com.example.ivitruk.vkadditionalclient.view.fragment.FollowersFragment;
import com.example.ivitruk.vkadditionalclient.view.fragment.FriendsFragment;
import com.example.ivitruk.vkadditionalclient.view.fragment.MessagesFragment;
import com.example.ivitruk.vkadditionalclient.view.fragment.UsersSearchFragment;
import com.example.ivitruk.vkadditionalclient.view.viewholder.DrawerViewHolder;
import com.example.ivitruk.vkadditionalclient.view.viewholder.MessageItemViewHolder;
import com.example.ivitruk.vkadditionalclient.view.viewholder.UserItemViewHolder;

import dagger.Subcomponent;

@ViewScope
@Subcomponent(modules = ViewModelModule.class)
public interface ViewModelComponent {

    void inject(LoginActivity loginActivity);

    void inject(MainActivity mainActivity);

    void inject(MessageItemViewHolder messageItemViewHolder);

    void inject(UserItemViewHolder userItemViewHolder);

    void inject(SplashActivity splashActivity);

    void inject(MessagesFragment messagesFragment);

    void inject(UsersSearchFragment usersSearchFragment);

    void inject(DrawerViewHolder drawerViewHolder);

    void inject(FriendsFragment friendsFragment);

    void inject(FollowersFragment followersFragment);
}
