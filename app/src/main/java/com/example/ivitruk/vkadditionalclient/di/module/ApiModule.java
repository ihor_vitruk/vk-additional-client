package com.example.ivitruk.vkadditionalclient.di.module;

import com.example.ivitruk.vkadditionalclient.model.api.ApiData;
import com.example.ivitruk.vkadditionalclient.model.api.IAuthApi;
import com.example.ivitruk.vkadditionalclient.model.api.IFriendsApi;
import com.example.ivitruk.vkadditionalclient.model.api.ILikesApi;
import com.example.ivitruk.vkadditionalclient.model.api.IMessagesApi;
import com.example.ivitruk.vkadditionalclient.model.api.IUsersApi;
import com.example.ivitruk.vkadditionalclient.di.scope.BusinessScope;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    @BusinessScope
    IMessagesApi providesMessagesApi(Retrofit retrofit) {
        return retrofit.create(IMessagesApi.class);
    }

    @Provides
    @BusinessScope
    IAuthApi providesAuthApi(OkHttpClient httpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiData.VK_API_HOST)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        return retrofit.create(IAuthApi.class);
    }

    @Provides
    @BusinessScope
    IUsersApi providesUsersApi(Retrofit retrofit) {
        return retrofit.create(IUsersApi.class);
    }

    @Provides
    @BusinessScope
    IFriendsApi providesFriendsApi(Retrofit retrofit) {
        return retrofit.create(IFriendsApi.class);
    }

    @Provides
    @BusinessScope
    ILikesApi providesLikesApi(Retrofit retrofit) {
        return retrofit.create(ILikesApi.class);
    }
}
