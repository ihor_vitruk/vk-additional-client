package com.example.ivitruk.vkadditionalclient.di.module;

import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.impl.FriendsBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.api.IAuthApi;
import com.example.ivitruk.vkadditionalclient.model.api.IFriendsApi;
import com.example.ivitruk.vkadditionalclient.model.api.ILikesApi;
import com.example.ivitruk.vkadditionalclient.model.api.IMessagesApi;
import com.example.ivitruk.vkadditionalclient.model.api.IUsersApi;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IFriendsBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.ILikesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IMessagesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IUsersBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.impl.AuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.impl.LikesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.impl.MessagesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.impl.UsersBusinessLayer;
import com.example.ivitruk.vkadditionalclient.di.scope.ViewModelScope;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;

import dagger.Module;
import dagger.Provides;

@Module
public class BusinessModule {

    @Provides
    @ViewModelScope
    IAuthBusinessLayer providesAuthBusinessLayer(IAuthApi authApi, ISessionManager sessionManager,
                                                 IInfoManager infoManager) {
        return new AuthBusinessLayer(authApi, sessionManager, infoManager);
    }

    @Provides
    @ViewModelScope
    IMessagesBusinessLayer providesMessagesBusinessLayer(IMessagesApi messagesApi, ISessionManager sessionManager) {
        return new MessagesBusinessLayer(messagesApi, sessionManager);
    }

    @Provides
    @ViewModelScope
    IUsersBusinessLayer providesUsersBusinessLayer(IUsersApi usersApi, ISessionManager sessionManager) {
        return new UsersBusinessLayer(usersApi, sessionManager);
    }

    @Provides
    @ViewModelScope
    IFriendsBusinessLayer providesFriendsBusinessLayer(IFriendsApi friendsApi, ISessionManager sessionManager) {
        return new FriendsBusinessLayer(friendsApi, sessionManager);
    }

    @Provides
    @ViewModelScope
    ILikesBusinessLayer providesLikesBusinessLayer(ILikesApi likesApi, ISessionManager sessionManager) {
        return new LikesBusinessLayer(likesApi, sessionManager);
    }
}
