package com.example.ivitruk.vkadditionalclient.manager;

import android.app.Activity;

public interface IActivityNavigationManager extends IManager {

    <T extends Activity> void startNew(Class<T> activityClass);

    <T extends Activity> void startSingle(Class<T> activityClass);
}
