package com.example.ivitruk.vkadditionalclient.manager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public interface IFragmentNavigationManager {

    void replaceFragment(int containerResId, Class<? extends Fragment> fragmentClass, FragmentManager fragmentManager);

    <T extends Fragment> Fragment getFragment(Class<T> fragmentClass, FragmentManager fragmentManager);
}
