package com.example.ivitruk.vkadditionalclient.manager;

import com.example.ivitruk.vkadditionalclient.manager.data.Session;

public interface ISessionManager extends IManager {

    String DEFAULT_PREF_STRING = "";
    long DEFAULT_PREF_LONG = -1;

    void saveSession(Session session);

    void deleteSession();

    boolean isValidSession();

    String getAccessToken();
}
