package com.example.ivitruk.vkadditionalclient.manager.data;

import android.os.SystemClock;
import android.text.TextUtils;

import com.example.ivitruk.vkadditionalclient.manager.impl.SessionManager;

public final class Session {

    private String accessToken;

    private long expiresIn;

    private String userId;

    private long accessTime;

    public long getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(long accessTime) {
        this.accessTime = accessTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public boolean isValid() {
        if (TextUtils.isEmpty(accessToken) ||
                SessionManager.DEFAULT_PREF_LONG == expiresIn ||
                TextUtils.isEmpty(userId) ||
                SessionManager.DEFAULT_PREF_LONG == accessTime) {
            return false;
        }

        if (expiresIn == 0) {
            return true;
        }

        long currentTime = SystemClock.currentThreadTimeMillis();
        long expiredTime = (currentTime - accessTime) / 1000;
        return expiredTime < expiresIn;
    }

    public static Session parseFromOauthCallbackUrl(String url) {
        Session session = new Session();
        String[] query = url.split("#");
        String[] params = query[1].split("&");
        session.setAccessToken(params[0].split("=")[1]);
        session.setExpiresIn(Long.valueOf(params[1].split("=")[1]));
        session.setUserId(params[2].split("=")[1]);
        session.setAccessTime(SystemClock.currentThreadTimeMillis());
        return session;
    }
}
