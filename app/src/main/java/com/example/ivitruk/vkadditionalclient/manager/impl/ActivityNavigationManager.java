package com.example.ivitruk.vkadditionalclient.manager.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.example.ivitruk.vkadditionalclient.App;
import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.view.activity.BaseActivity;

public class ActivityNavigationManager implements IActivityNavigationManager {

    private Context context;

    public ActivityNavigationManager(Context context) {
        this.context = context;
    }

    @Override
    public <T extends Activity> void startNew(Class<T> activityClass) {
        Intent intent = new Intent(context, activityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    public <T extends Activity> void startSingle(Class<T> activityClass) {
        startNew(activityClass);
        BaseActivity currentActivity = App.getCurrentActivity();
        if (currentActivity != null) {
            currentActivity.finish();
        }
    }
}
