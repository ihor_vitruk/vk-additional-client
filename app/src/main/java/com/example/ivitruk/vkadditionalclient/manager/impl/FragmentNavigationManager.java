package com.example.ivitruk.vkadditionalclient.manager.impl;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.ivitruk.vkadditionalclient.manager.IFragmentNavigationManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class FragmentNavigationManager implements IFragmentNavigationManager {
    @Override
    public void replaceFragment(int containerResId, Class<? extends Fragment> fragmentClass, FragmentManager fragmentManager) {
        String tag = fragmentClass.getCanonicalName();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerResId, getFragmentByClass(fragmentClass), tag);
        fragmentTransaction.commit();
    }

    @Override
    public <T extends Fragment> Fragment getFragment(Class<T> fragmentClass, FragmentManager fragmentManager) {
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments == null) {
            return null;
        }
        for (Fragment fmFragment : fragments) {
            if (fmFragment != null && fmFragment.getClass().equals(fragmentClass) &&
                    fmFragment.isVisible()) {
                return fmFragment;
            }
        }
        return null;
    }

    private Fragment getFragmentByClass(Class<? extends Fragment> fragmentClass) {
        Constructor<?> constructor;
        try {
            constructor = fragmentClass.getConstructor();
            return (Fragment) constructor.newInstance();
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            return null;
        }
    }
}
