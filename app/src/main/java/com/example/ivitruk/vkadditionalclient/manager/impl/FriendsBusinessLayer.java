package com.example.ivitruk.vkadditionalclient.manager.impl;

import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.api.ApiData;
import com.example.ivitruk.vkadditionalclient.model.api.IFriendsApi;
import com.example.ivitruk.vkadditionalclient.model.business.IFriendsBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.impl.BaseBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.FriendsDataListener;

public class FriendsBusinessLayer extends BaseBusinessLayer<FriendsDataListener, IFriendsApi> implements IFriendsBusinessLayer {

    public FriendsBusinessLayer(IFriendsApi api, ISessionManager sessionManager) {
        super(api, sessionManager);
    }

    @Override
    public void searchFriends(String query, int count, int offset) {
        doMethod(api.searchFriends(query, count, offset, "hints", ApiData.DEFAULT_FIELDS),
                responseObject -> dataListener.onGetFriendsSuccess(responseObject.getResponse().getUsers()),
                throwable -> dataListener.onGetFriendsError(throwable));
    }
}
