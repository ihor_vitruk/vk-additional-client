package com.example.ivitruk.vkadditionalclient.model.api;

public final class ApiData {

    private static final String scope = "messages,friends,offline,wall";

    public static final String CLIENT_ID = "5431525";
    public static final String VK_API_HOST = "https://api.vk.com/";
    public static final String OAUTH_CALLBACK_URL = VK_API_HOST + "blank.html";
    public static final String OAUTH_AUTHORIZE_URL = VK_API_HOST +
            "oauth/authorize?client_id=" + CLIENT_ID + "&scope=" + scope +
            "&redirect_uri=http://api.vk.com/blank.html&display=touch&response_type=token";

    public static final String OAUTH_LOGOUT_URL = VK_API_HOST + "oauth/logout?client_id=" + CLIENT_ID;
    public static final String VK_API_URL = VK_API_HOST + "method/";

    public static final String API_VERSION = "5.52";

    public static final String DEFAULT_FIELDS = "relation, photo_100, " +
            "can_write_private_message, can_send_friend_request, " +
            "blacklisted, blacklisted_by_me, friend_status, photo_id";

    public static final int DEFAULT_PAGE_COUNT = 20;

    public static class ApiErrorCodes {

        public static final int AUTHORIZATION_FAILED = 5;
    }
}
