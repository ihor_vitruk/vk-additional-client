package com.example.ivitruk.vkadditionalclient.model.api;

import com.example.ivitruk.vkadditionalclient.model.api.response.MessagesRestoreResponseObject;

import retrofit2.http.GET;
import rx.Observable;

public interface IAuthApi {

    @GET("oauth/logout?client_id=" + ApiData.CLIENT_ID)
    Observable<MessagesRestoreResponseObject> logout();
}
