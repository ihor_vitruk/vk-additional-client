package com.example.ivitruk.vkadditionalclient.model.api;

import com.example.ivitruk.vkadditionalclient.model.api.response.UsersSearchResponseObject;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface IFriendsApi {

    @GET("friends.search")
    Observable<UsersSearchResponseObject> searchFriends(@Query("q") String query,
                                                        @Query("count") int count,
                                                        @Query("offset") int offset,
                                                        @Query("order") String order,
                                                        @Query("fields") String fields);
}
