package com.example.ivitruk.vkadditionalclient.model.api;

import com.example.ivitruk.vkadditionalclient.model.api.response.LikesResponseObject;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ILikesApi {

    @GET("likes.add")
    Observable<LikesResponseObject> add(@Query("type") String photo,
                                        @Query("owner_id") long ownerId,
                                        @Query("item_id") long itemId);
}
