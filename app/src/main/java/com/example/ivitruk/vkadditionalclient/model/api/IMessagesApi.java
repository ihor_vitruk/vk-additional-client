package com.example.ivitruk.vkadditionalclient.model.api;

import com.example.ivitruk.vkadditionalclient.model.api.response.MessagesRestoreResponseObject;
import com.example.ivitruk.vkadditionalclient.model.api.response.MessagesResponseObject;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface IMessagesApi {

    @GET("messages.get")
    Observable<MessagesResponseObject> getMessages(@Query("count") int count);

    @GET("messages.getById")
    Observable<MessagesResponseObject> getMessagesById(@Query("message_ids") String messageIds);

    @GET("messages.restore")
    Observable<MessagesRestoreResponseObject> restoreMessage(@Query("message_id") String messageId);

    @GET("messages.getHistory")
    Observable<MessagesResponseObject> getHistory(@Query("user_id") String userId,
                                                  @Query("count") int count);
}
