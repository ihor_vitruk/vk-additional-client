package com.example.ivitruk.vkadditionalclient.model.api;

import com.example.ivitruk.vkadditionalclient.model.api.response.ExtendedUserSearchResponseObject;
import com.example.ivitruk.vkadditionalclient.model.api.response.UsersSearchResponseObject;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface IUsersApi {

    @GET("users.search")
    Observable<UsersSearchResponseObject> search(@Query("q") String query,
                                                 @Query("count") int count,
                                                 @Query("fields") String fields,
                                                 @Query("city") long city,
                                                 @Query("country") long country,
                                                 @Query("sort") int sort,
                                                 @Query("sex") int sex,
                                                 @Query("status") int status,
                                                 @Query("age_from") int ageFrom,
                                                 @Query("age_to") int ageTo,
                                                 @Query("birth_day") int birthday,
                                                 @Query("birth_month") int birthMonth,
                                                 @Query("online") int online,
                                                 @Query("has_photo") int hasPhoto);

    @GET("users.getFollowers")
    Observable<UsersSearchResponseObject> getFollowers(@Query("count") int count,
                                                       @Query("offset") int offset,
                                                       @Query("fields") String fields);

    @GET("execute")
    Observable<ExtendedUserSearchResponseObject> search(@Query("code") String code);
}
