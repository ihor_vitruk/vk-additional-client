package com.example.ivitruk.vkadditionalclient.model.api.response;

import com.google.gson.annotations.SerializedName;

public final class Error {

    @SerializedName("error_code")
    private final int errorCode;

    @SerializedName("error_msg")
    private final String errorMessage;

    public Error(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String  getErrorMessage() {
        return errorMessage;
    }
}
