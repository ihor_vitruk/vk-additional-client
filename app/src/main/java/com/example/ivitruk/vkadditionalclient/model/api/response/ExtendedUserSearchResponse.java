package com.example.ivitruk.vkadditionalclient.model.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExtendedUserSearchResponse {

    @SerializedName("all")
    private final UsersSearchResponse usersSearchResponse;

    @SerializedName("with_statuses")
    private final List<Long> withStatuses;

    public ExtendedUserSearchResponse(UsersSearchResponse usersSearchResponse,
                                      List<Long> withStatuses) {
        this.usersSearchResponse = usersSearchResponse;
        this.withStatuses = withStatuses;
    }

    public UsersSearchResponse getUsersSearchResponse() {
        return usersSearchResponse;
    }

    public List<Long> getWithStatuses() {
        return withStatuses;
    }
}
