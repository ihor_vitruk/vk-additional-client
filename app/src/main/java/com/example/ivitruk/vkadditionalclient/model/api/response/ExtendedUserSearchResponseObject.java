package com.example.ivitruk.vkadditionalclient.model.api.response;

public class ExtendedUserSearchResponseObject extends ResponseObject<ExtendedUserSearchResponse> {
    public ExtendedUserSearchResponseObject(ExtendedUserSearchResponse response, Error error) {
        super(response, error);
    }
}
