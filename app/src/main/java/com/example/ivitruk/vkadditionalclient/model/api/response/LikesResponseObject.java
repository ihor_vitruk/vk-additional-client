package com.example.ivitruk.vkadditionalclient.model.api.response;

public class LikesResponseObject extends ResponseObject<LikesResponse> {
    public LikesResponseObject(LikesResponse response, Error error) {
        super(response, error);
    }
}
