package com.example.ivitruk.vkadditionalclient.model.api.response;

import com.google.gson.annotations.SerializedName;

public final class Message {

    @SerializedName("mid")
    private final long id;

    @SerializedName("uid")
    private final long userId;

    @SerializedName("body")
    private final String body;

    @SerializedName("out")
    private final byte out;

    public Message(long id, long userId, String body, byte out) {
        this.id = id;
        this.userId = userId;
        this.body = body;
        this.out = out;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public String getBody() {
        return body;
    }

    public byte getOut() {
        return out;
    }

    public boolean isOutput() {
        return out == 1;
    }
}
