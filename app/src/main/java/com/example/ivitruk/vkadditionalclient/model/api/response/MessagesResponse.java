package com.example.ivitruk.vkadditionalclient.model.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class MessagesResponse {

    @SerializedName("count")
    private final int count;

    @SerializedName("items")
    private final List<Message> messages;

    public MessagesResponse(int count, List<Message> messages) {
        this.count = count;
        this.messages = messages;
    }

    public int getCount() {
        return count;
    }

    public List<Message> getMessages() {
        return messages;
    }
}
