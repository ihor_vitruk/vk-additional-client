package com.example.ivitruk.vkadditionalclient.model.api.response;

public class MessagesResponseObject extends ResponseObject<MessagesResponse> {
    public MessagesResponseObject(MessagesResponse response, Error error) {
        super(response, error);
    }
}
