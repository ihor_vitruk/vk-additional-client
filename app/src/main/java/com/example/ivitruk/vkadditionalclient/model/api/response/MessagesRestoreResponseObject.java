package com.example.ivitruk.vkadditionalclient.model.api.response;

import java.util.Map;

public class MessagesRestoreResponseObject extends ResponseObject<Map<Long, Integer>> {
    public MessagesRestoreResponseObject(Map<Long, Integer> response, Error error) {
        super(response, error);
    }
}
