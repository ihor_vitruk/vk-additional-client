package com.example.ivitruk.vkadditionalclient.model.api.response;

import com.example.ivitruk.vkadditionalclient.PaginationRvItem;
import com.google.gson.annotations.SerializedName;

public final class User implements PaginationRvItem {

    @SerializedName("id")
    private final long id;

    @SerializedName("first_name")
    private final String firstName;

    @SerializedName("last_name")
    private final String lastName;

    @SerializedName("photo_100")
    private final String photo;

    @SerializedName("relation")
    private final byte relation;

    @SerializedName("can_write_private_message")
    private final int canWritePrivateMessage;

    @SerializedName("can_send_friend_request")
    private final int canSendFriendRequest;

    @SerializedName("blacklisted")
    private final int blacklisted;

    @SerializedName("blacklisted_by_me")
    private final int blacklistedByMe;

    @SerializedName("friend_status")
    private final int friendStatus;

    @SerializedName("photo_id")
    private final String photoId;

    public User(long id, String firstName, String lastName, String photo,
                byte relation, int canWritePrivateMessage, int canSendFriendRequest,
                int blacklisted, int blacklistedByMe, int friendStatus, String photoId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo = photo;
        this.relation = relation;
        this.canWritePrivateMessage = canWritePrivateMessage;
        this.canSendFriendRequest = canSendFriendRequest;
        this.blacklisted = blacklisted;
        this.blacklistedByMe = blacklistedByMe;
        this.friendStatus = friendStatus;
        this.photoId = photoId;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoto() {
        return photo;
    }

    public byte getRelation() {
        return relation;
    }

    public int getCanWritePrivateMessage() {
        return canWritePrivateMessage;
    }

    public int getBlacklisted() {
        return blacklisted;
    }

    public int getBlacklistedByMe() {
        return blacklistedByMe;
    }

    public int getCanSendFriendRequest() {
        return canSendFriendRequest;
    }

    public int getFriendStatus() {
        return friendStatus;
    }

    public String getPhotoId() {
        return photoId;
    }
}
