package com.example.ivitruk.vkadditionalclient.model.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UsersSearchResponse {

    @SerializedName("count")
    private final int count;

    @SerializedName("items")
    private final List<User> users;

    public UsersSearchResponse(int count, List<User> users) {
        this.count = count;
        this.users = users;
    }

    public int getCount() {
        return count;
    }

    public List<User> getUsers() {
        return users;
    }
}
