package com.example.ivitruk.vkadditionalclient.model.api.response;

public class UsersSearchResponseObject extends ResponseObject<UsersSearchResponse> {
    public UsersSearchResponseObject(UsersSearchResponse response, Error error) {
        super(response, error);
    }
}
