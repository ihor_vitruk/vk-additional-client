package com.example.ivitruk.vkadditionalclient.model.business;

import android.webkit.WebView;

import com.example.ivitruk.vkadditionalclient.model.datalistener.AuthDataListener;

public interface IAuthBusinessLayer extends IBusinessLayer<AuthDataListener> {

    void login(WebView webView);

    void logout();

    boolean isLoggedIn();
}
