package com.example.ivitruk.vkadditionalclient.model.business;

import com.example.ivitruk.vkadditionalclient.model.datalistener.BaseDataListener;

public interface IBusinessLayer<T extends BaseDataListener> {

    void destroy();

    void setDataListener(T dataListener);
}
