package com.example.ivitruk.vkadditionalclient.model.business;

import com.example.ivitruk.vkadditionalclient.model.datalistener.FriendsDataListener;

public interface IFriendsBusinessLayer extends IBusinessLayer<FriendsDataListener> {

    void searchFriends(String query, int count, int offset);
}
