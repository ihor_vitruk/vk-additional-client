package com.example.ivitruk.vkadditionalclient.model.business;

import com.example.ivitruk.vkadditionalclient.model.datalistener.LikesDataListener;

public interface ILikesBusinessLayer extends IBusinessLayer<LikesDataListener> {
    void add(String type,
             long ownerId,
             long itemId);
}
