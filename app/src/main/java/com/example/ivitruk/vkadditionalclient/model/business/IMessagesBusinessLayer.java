package com.example.ivitruk.vkadditionalclient.model.business;

import android.support.annotation.NonNull;

import com.example.ivitruk.vkadditionalclient.model.datalistener.MessagesDataListener;

public interface IMessagesBusinessLayer extends IBusinessLayer<MessagesDataListener> {

    void getMessages(int count);

    void getMessagesById(@NonNull String[] messageIds);

    void restoreMessage(String messageId);

    void getHistory(String userId, int count);
}
