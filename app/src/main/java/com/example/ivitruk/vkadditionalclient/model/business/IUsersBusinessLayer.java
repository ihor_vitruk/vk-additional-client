package com.example.ivitruk.vkadditionalclient.model.business;

import com.example.ivitruk.vkadditionalclient.model.datalistener.UsersDataListener;

public interface IUsersBusinessLayer extends IBusinessLayer<UsersDataListener> {

    void search(String query,
                String fields,
                long city,
                long country,
                int sort,
                int status,
                int sex,
                int ageFrom,
                int ageTo,
                int birthDay,
                int birthMonth,
                int online,
                int hasPhoto,
                int canWritePrivateMessage,
                int canSendFriendRequest,
                int blacklisted,
                int blacklistedByMe,
                int friendStatus
    );

    void getFollowers(int count, int offset);
}
