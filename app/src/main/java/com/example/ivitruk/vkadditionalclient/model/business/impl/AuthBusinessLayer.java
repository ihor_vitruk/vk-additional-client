package com.example.ivitruk.vkadditionalclient.model.business.impl;

import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.manager.data.Session;
import com.example.ivitruk.vkadditionalclient.model.api.ApiData;
import com.example.ivitruk.vkadditionalclient.model.api.IAuthApi;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.AuthDataListener;
import com.example.ivitruk.vkadditionalclient.viewmodel.LoginViewModel;

public class AuthBusinessLayer extends BaseBusinessLayer<AuthDataListener, IAuthApi> implements IAuthBusinessLayer {

    private IInfoManager infoManager;

    public AuthBusinessLayer(IAuthApi api, ISessionManager sessionManager,
                             IInfoManager infoManager) {
        super(api, sessionManager);
        this.infoManager = infoManager;
    }

    @Override
    public void login(WebView webView) {
        setupWebView(webView);
    }

    @Override
    public void logout() {
        removeCookies();
        doMethod(api.logout(),
                responseObject -> dataListener.onLogoutSuccess(),
                throwable -> dataListener.onLogoutError(throwable));
    }

    @Override
    public boolean isLoggedIn() {
        return sessionManager.isValidSession();
    }

    private void setupWebView(WebView webView) {
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setWebViewClient(new VkLoginWebViewClient());
        webView.loadUrl(ApiData.OAUTH_AUTHORIZE_URL);
    }

    private final class VkLoginWebViewClient extends WebViewClient {

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            infoManager.hideProgress();
            dataListener.onLoginError(new Throwable(description));
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            //Log.d(Constants.DEBUG_TAG, "Loading URL: " + url);
            super.onPageStarted(view, url, favicon);
            infoManager.hideProgress();
            if (url.contains("access_token")) {
                if (!sessionManager.isValidSession()) {
                    dataListener.onLoginSuccess(Session.parseFromOauthCallbackUrl(url));
                }
            }
        }
    }

    private void removeCookies() {
        android.webkit.CookieManager cookieManager = CookieManager.getInstance();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeAllCookies(aBoolean -> Log.d("TAG", "Cookie removed: " + aBoolean));
        } else {
            cookieManager.removeAllCookie();
        }
    }
}
