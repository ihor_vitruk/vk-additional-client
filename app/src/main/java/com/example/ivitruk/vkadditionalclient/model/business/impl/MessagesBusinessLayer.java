package com.example.ivitruk.vkadditionalclient.model.business.impl;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.example.ivitruk.vkadditionalclient.model.api.IMessagesApi;
import com.example.ivitruk.vkadditionalclient.model.business.IMessagesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.MessagesDataListener;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;

public class MessagesBusinessLayer extends BaseBusinessLayer<MessagesDataListener, IMessagesApi> implements IMessagesBusinessLayer {

    public MessagesBusinessLayer(IMessagesApi api, ISessionManager sessionManager) {
        super(api, sessionManager);
    }

    @Override
    public void getMessages(int count) {
        doMethod(api.getMessages(count),
                responseObject -> dataListener.onGetMessagesSuccess(responseObject.getResponse().getMessages()),
                throwable -> dataListener.onGetMessagesError(throwable)
        );
    }

    @Override
    public void getMessagesById(@NonNull String[] messageIds) {
        doMethod(api.getMessagesById(TextUtils.join(",", messageIds)),
                responseObject -> dataListener.onGetMessagesByIdSuccess(responseObject.getResponse().getMessages()),
                throwable -> dataListener.onGetMessagesByIdError(throwable));
    }

    @Override
    public void restoreMessage(String messageId) {
        doMethod(api.restoreMessage(messageId),
                responseObject -> dataListener.onRestoreSuccess(responseObject.getResponse()),
                throwable -> dataListener.onRestoreError(throwable));
    }

    @Override
    public void getHistory(String userId, int count) {
        doMethod(api.getHistory(userId, count),
                responseObject -> dataListener.onGetHistorySuccess(responseObject.getResponse().getMessages()),
                throwable -> dataListener.onGetHistoryError(throwable));
    }
}
