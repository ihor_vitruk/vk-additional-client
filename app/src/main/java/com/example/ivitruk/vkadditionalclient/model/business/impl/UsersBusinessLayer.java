package com.example.ivitruk.vkadditionalclient.model.business.impl;

import android.text.TextUtils;

import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.api.ApiData;
import com.example.ivitruk.vkadditionalclient.model.api.IUsersApi;
import com.example.ivitruk.vkadditionalclient.model.api.response.User;
import com.example.ivitruk.vkadditionalclient.model.business.IUsersBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.UsersDataListener;

import java.util.ArrayList;
import java.util.List;

public class UsersBusinessLayer extends BaseBusinessLayer<UsersDataListener, IUsersApi> implements IUsersBusinessLayer {

    public UsersBusinessLayer(IUsersApi api, ISessionManager sessionManager) {
        super(api, sessionManager);
    }

    @Override
    public void search(String query, String fields, long city, long country, int sort, int status, int sex,
                       int ageFrom, int ageTo, int birthDay, int birthMonth, int online,
                       int hasPhoto, int canWritePrivateMessage, int canSendFriendRequest,
                       int blacklisted, int blacklistedByMe, int friendStatus) {
        if (status < 8) {
            doMethod(api.search(query, 1000, fields, city, country, sort, sex, status >= 0 ? status : 0, ageFrom, ageTo, birthDay, birthMonth, online, hasPhoto),
                    responseObject -> {
                        List<User> allUsers = responseObject.getResponse().getUsers();
                        //And now loal filter users for search parameters (can write, can add to friend, etc)
                        List<User> localFiltered = localFilters(allUsers, canWritePrivateMessage,
                                canSendFriendRequest, blacklisted, blacklistedByMe, friendStatus);

                        dataListener.onSearchUsersSuccess(localFiltered);
                    },
                    throwable -> dataListener.onSearchUsersError(throwable));
        } else {
            doMethod(api.search(generateJsCodeForMethod(query, fields, city, country, sort, sex, ageFrom, ageTo,
                    birthDay, birthMonth, online, hasPhoto)),
                    responseObject -> {
                        List<User> allUsers = responseObject.getResponse().getUsersSearchResponse().getUsers();
                        List<Long> withStatusesIds = responseObject.getResponse().getWithStatuses();

                        //First filter with ids with Statuses received from server;
                        List<User> filtered1 = filterByIdsWithStatuses(allUsers, withStatusesIds);

                        //Also we need to filter on received relation one more time, because
                        //sometimes server returns wrong ids with statuses (not all);
                        List<User> filtered2 = filterWithoutRelation(filtered1);

                        //And now local filter users for search parameters (can write, can add to friend, etc)
                        List<User> localFiltered = localFilters(filtered2, canWritePrivateMessage,
                                canSendFriendRequest, blacklisted, blacklistedByMe, friendStatus);

                        dataListener.onSearchUsersSuccess(localFiltered);
                    },
                    throwable -> dataListener.onSearchUsersError(throwable));
        }
    }

    @Override
    public void getFollowers(int count, int offset) {
        doMethod(api.getFollowers(count, offset, ApiData.DEFAULT_FIELDS),
                responseObject -> dataListener.onGetFollowersSuccess(responseObject.getResponse().getUsers()),
                throwable -> dataListener.onGetFollowersError(throwable));
    }

    /**
     * Example:
     * var all = API.users.search({count:1000, sort:0,
     * country:2, city:1057, sex:1, age_from:18, fields: "can_write_private_message,
     * can_send_friend_request", age_to:18, online:1, has_photo:1});
     * var allWithStatusesIds = [];
     * int i = 1;
     * while (i <=7) {
     * var userIds = API.users.search({count:1000, sort:0, country:2, city:1057,
     * sex:1, status:i, age_from:18, age_to:18, online:1, has_photo:1}).items@.id;
     * i = i + 1;
     * allWithStatusesIds = allWithStatusesIds + userIds;
     * }
     * return {all:all, with_statuses:allWithStatusesIds};
     */
    private String generateJsCodeForMethod(String query, String fields, long city, long country, int sort, int sex, int ageFrom, int ageTo, int birthDay, int birthMonth, int online, int hasPhoto) {
        StringBuilder commonSearchStringSb = new StringBuilder();

        commonSearchStringSb.append("API.users.search({count:1000, sort:").append(sort);
        if (!TextUtils.isEmpty(query)) {
            commonSearchStringSb.append(", q:\"").append(query).append("\"");
        }
        commonSearchStringSb.append(", country:").append(country)
                .append(", city:").append(city).append(", sex:").append(sex)
                .append(", age_from:").append(ageFrom).append(", age_to:").append(ageTo)
                .append(", birth_day:").append(birthDay).append(", birth_month:").append(birthMonth)
                .append(", online:").append(online).append(", has_photo:").append(hasPhoto);
        String commonSearchString = commonSearchStringSb.toString();

        StringBuilder code = new StringBuilder();
        code.append("var all = ").append(commonSearchString).append(", fields:\"").append(fields)
                .append("\"});").append("\n");
        code.append("var allWithStatusesIds = [];").append("\n");
        code.append("int i = 1;").append("\n");
        code.append("while (i <=7) {").append("\n");
        code.append("var userIds = ").append(commonSearchString).append(", status:i}).items@.id;").append("\n");
        code.append("i = i + 1;").append("\n");
        code.append("allWithStatusesIds = allWithStatusesIds + userIds;").append("\n");
        code.append("}").append("\n");
        code.append("return {all:all, with_statuses:allWithStatusesIds};");
        return code.toString();
    }

    private List<User> filterByIdsWithStatuses(List<User> all, List<Long> idsWithStatuses) {
        List<User> result = new ArrayList<>();
        for (User user : all) {
            if (!idsWithStatuses.contains(user.getId())) {
                result.add(user);
            }
        }
        return result;
    }

    private List<User> filterWithoutRelation(List<User> list) {
        List<User> result = new ArrayList<>();
        for (User user : list) {
            if (user.getRelation() == 0) {
                result.add(user);
            }
        }
        return result;
    }

    private List<User> localFilters(List<User> users, int canWritePrivateMessage, int canSendFriendRequest,
                                    int blacklisted, int blacklistedByMe, int friendStatus) {
        return filterCanSendFriendRequest
                (filterCanWritePrivateMessage(
                        filterBlacklisted(
                                filterBlacklistedByMe(
                                        filterByFriendStatus(users, friendStatus),
                                        blacklistedByMe),
                                blacklisted),
                        canWritePrivateMessage),
                        canSendFriendRequest);

    }

    private List<User> filterCanWritePrivateMessage(List<User> users, int canWritePrivateMessage) {
        List<User> result = new ArrayList<>();
        for (User user : users) {
            if (canWritePrivateMessage != 1) {
                result.add(user);
            } else if (user.getCanWritePrivateMessage() == canWritePrivateMessage) {
                result.add(user);
            }
        }
        return result;
    }

    private List<User> filterCanSendFriendRequest(List<User> users, int canSendFriendRequest) {
        List<User> result = new ArrayList<>();
        for (User user : users) {
            if (canSendFriendRequest != 1) {
                result.add(user);
            } else if (user.getCanSendFriendRequest() == canSendFriendRequest) {
                result.add(user);
            }
        }
        return result;
    }

    private List<User> filterBlacklisted(List<User> users, int blacklisted) {
        List<User> result = new ArrayList<>();
        for (User user : users) {
            if (blacklisted == -1) {
                result.add(user);
            } else if (user.getBlacklisted() == blacklisted) {
                result.add(user);
            }
        }
        return result;
    }

    private List<User> filterBlacklistedByMe(List<User> users, int blacklistedByMe) {
        List<User> result = new ArrayList<>();
        for (User user : users) {
            if (blacklistedByMe == -1) {
                result.add(user);
            } else if (user.getBlacklistedByMe() == blacklistedByMe) {
                result.add(user);
            }
        }
        return result;
    }

    private List<User> filterByFriendStatus(List<User> users, int friendStatus) {
        List<User> result = new ArrayList<>();
        for (User user : users) {
            if (friendStatus == -1) {
                result.add(user);
            } else if (user.getFriendStatus() == friendStatus) {
                result.add(user);
            }
        }
        return result;
    }
}
