package com.example.ivitruk.vkadditionalclient.model.datalistener;

import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;

public abstract class BaseDataListener {

    private IAuthBusinessLayer authBusinessLayer;

    public BaseDataListener(IAuthBusinessLayer authBusinessLayer) {
        this.authBusinessLayer = authBusinessLayer;
    }

    public void onUnauthorizedError() {
        if (authBusinessLayer != null) {
            authBusinessLayer.logout();
        }
    }
}
