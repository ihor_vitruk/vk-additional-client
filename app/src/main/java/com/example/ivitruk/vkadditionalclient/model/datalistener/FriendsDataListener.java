package com.example.ivitruk.vkadditionalclient.model.datalistener;

import com.example.ivitruk.vkadditionalclient.model.api.response.User;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;

import java.util.List;

public abstract class FriendsDataListener extends BaseDataListener {
    public FriendsDataListener(IAuthBusinessLayer authBusinessLayer) {
        super(authBusinessLayer);
    }

    //users.get
    public void onGetFriendsSuccess(List<User> users) {
    }

    public void onGetFriendsError(Throwable t) {
    }
}
