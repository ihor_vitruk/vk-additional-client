package com.example.ivitruk.vkadditionalclient.model.datalistener;

import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;

public class LikesDataListener extends BaseDataListener {

    public LikesDataListener(IAuthBusinessLayer authBusinessLayer) {
        super(authBusinessLayer);
    }

    //likes.add
    public void onAddLikeSuccess() {
    }

    public void onAddLikeError(Throwable t) {
    }
}
