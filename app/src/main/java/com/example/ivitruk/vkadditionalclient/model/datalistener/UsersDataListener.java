package com.example.ivitruk.vkadditionalclient.model.datalistener;

import com.example.ivitruk.vkadditionalclient.model.api.response.User;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;

import java.util.List;

public class UsersDataListener extends BaseDataListener {

    public UsersDataListener(IAuthBusinessLayer authBusinessLayer) {
        super(authBusinessLayer);
    }

    public void onSearchUsersSuccess(List<User> users) {
    }

    public void onSearchUsersError(Throwable t) {
    }

    //users.getFollowers
    public void onGetFollowersSuccess(List<User> users) {
    }

    public void onGetFollowersError(Throwable t) {

    }
}
