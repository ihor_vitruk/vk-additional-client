package com.example.ivitruk.vkadditionalclient.util;

import android.databinding.BindingAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ImageView;

import com.example.ivitruk.vkadditionalclient.viewmodel.BaseViewModel;

/**
 * Here custom binding attributes for views can be found
 */
public class BindingUtil {

    @BindingAdapter("bind:image_url")
    public static void loadImage(ImageView imageView, String url) {
        UiUtil.showImage(imageView, url);
    }

    @BindingAdapter("bind:refresh")
    public static void swipeToRefresh(SwipeRefreshLayout swipeRefreshLayout, BaseViewModel viewModel) {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(false);
            viewModel.refreshData();
        });
    }

    private BindingUtil() {
        throw new AssertionError();
    }
}
