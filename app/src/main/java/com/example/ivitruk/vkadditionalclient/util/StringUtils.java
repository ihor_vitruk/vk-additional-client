package com.example.ivitruk.vkadditionalclient.util;

import android.text.TextUtils;

public class StringUtils {

    public static long safeLong(String string, long defaultValue) {
        if (TextUtils.isEmpty(string)) {
            return defaultValue;
        } else {
            try {
                return Long.valueOf(string);
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        }
    }

    public static String safeString(String string, String defaultValue) {
        if (TextUtils.isEmpty(string)) {
            return defaultValue;
        }
        return string;
    }

    private StringUtils() {
        throw new AssertionError();
    }
}
