package com.example.ivitruk.vkadditionalclient.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.ivitruk.vkadditionalclient.App;
import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.LoginActivityBinding;
import com.example.ivitruk.vkadditionalclient.viewmodel.LoginViewModel;

import javax.inject.Inject;

public class LoginActivity extends BaseActivity<LoginViewModel, LoginActivityBinding> {

    @Inject
    LoginViewModel loginViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel.prepareForLogin(binding.wvLogin);
    }

    @NonNull
    @Override
    public LoginViewModel initViewModel() {
        viewModelComponent.inject(this);
        return loginViewModel;
    }

    @NonNull
    @Override
    public LoginActivityBinding initBinding() {
        LoginActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.login_activity);
        binding.setViewModel(viewModel);
        return binding;
    }


}
