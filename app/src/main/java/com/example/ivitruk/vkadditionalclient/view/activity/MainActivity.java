package com.example.ivitruk.vkadditionalclient.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.MenuItem;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.MainActivityBinding;
import com.example.ivitruk.vkadditionalclient.databinding.UserSearchFragmentBinding;
import com.example.ivitruk.vkadditionalclient.view.fragment.UsersSearchFragment;
import com.example.ivitruk.vkadditionalclient.viewmodel.MainViewModel;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<MainViewModel, MainActivityBinding> {

    @Inject
    MainViewModel mainViewModel;

    private ActionBarDrawerToggle toggle;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        toggle = new ActionBarDrawerToggle(this, binding.mainDrawerLayout, R.string.open_drawer, R.string.close_drawer);
        binding.mainDrawerLayout.addDrawerListener(toggle);
        viewModel.setupDrawer(binding.mainDrawerLayout,
                binding.drawerRv,
                actionBar,
                viewModelComponent,
                getSupportFragmentManager());
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                if (binding.mainDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    binding.mainDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    binding.mainDrawerLayout.openDrawer(Gravity.LEFT);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = viewModel.fragmentNavigationManager
                .getFragment(UsersSearchFragment.class, getSupportFragmentManager());
        if (fragment != null && fragment.isVisible()) {
            UsersSearchFragment usersSearchFragment = (UsersSearchFragment) fragment;
            UserSearchFragmentBinding usfb = usersSearchFragment.getBinding();
            if (usfb.userSearchParametersContent.isExpanded()) {
                usfb.userSearchParametersContent.collapse();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @NonNull
    @Override
    public MainViewModel initViewModel() {
        viewModelComponent.inject(this);
        return mainViewModel;
    }

    @NonNull
    @Override
    public MainActivityBinding initBinding() {
        MainActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        binding.setViewModel(viewModel);
        return binding;
    }
}
