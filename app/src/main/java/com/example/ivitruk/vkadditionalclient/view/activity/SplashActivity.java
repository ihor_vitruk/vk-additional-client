package com.example.ivitruk.vkadditionalclient.view.activity;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.SplashActivityBinding;
import com.example.ivitruk.vkadditionalclient.viewmodel.SplashViewModel;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity<SplashViewModel, SplashActivityBinding> {

    @Inject
    SplashViewModel splashViewModel;

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.processAuthorization();
    }

    @NonNull
    @Override
    public SplashViewModel initViewModel() {
        viewModelComponent.inject(this);
        return splashViewModel;
    }

    @NonNull
    @Override
    public SplashActivityBinding initBinding() {
        SplashActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.splash_activity);
        binding.setViewModel(viewModel);
        return binding;
    }
}
