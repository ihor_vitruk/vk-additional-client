package com.example.ivitruk.vkadditionalclient.view.adapter;

import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ivitruk.vkadditionalclient.PaginationRvItem;
import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.PaginationRvFooterBinding;
import com.example.ivitruk.vkadditionalclient.di.component.ViewModelComponent;
import com.example.ivitruk.vkadditionalclient.model.api.ApiData;
import com.example.ivitruk.vkadditionalclient.view.listener.PaginationRvScrollListener;
import com.example.ivitruk.vkadditionalclient.view.viewholder.BaseViewHolder;

import java.util.List;

public abstract class BasePaginationRvAdapter<T extends PaginationRvItem, VH extends BaseViewHolder> extends BaseRvAdapter<PaginationRvItem, BaseViewHolder> {

    private static final int TYPE_DATA = 0;
    private static final int TYPE_FOOTER = 1;

    private int pageCount = ApiData.DEFAULT_PAGE_COUNT;

    private int pageOffset;

    private boolean lastPageReached;

    private PageLoader pageLoader;

    private PaginationRvScrollListener listener = new PaginationRvScrollListener() {
        @Override
        public void onScrolledToBottom() {
            if (!lastPageReached) {
                showLoadingBar();
            }
        }
    };

    public BasePaginationRvAdapter(ViewModelComponent viewModelComponent) {
        super(viewModelComponent);
    }

    public abstract VH onCreateDataViewHolder(ViewGroup parent, int viewType);

    public abstract void onBindDataViewHolder(VH holder, T item);

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_FOOTER) {
            PaginationRvFooterBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.pagination_rv_footer, parent, false);
            return new FooterViewHolder(binding);
        } else {
            return onCreateDataViewHolder(parent, viewType);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        PaginationRvItem item = items.get(position);
        if (!(item instanceof PaginationRvFooter)) {
            this.onBindDataViewHolder((VH) holder, (T) items.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof PaginationRvFooter) {
            return TYPE_FOOTER;
        }
        return TYPE_DATA;
    }

    public void resetData() {
        pageOffset = 0;
        lastPageReached = false;
        items.clear();
        notifyDataSetChanged();
    }

    public void onDataReceived(List<T> items) {
        if (items.size() < pageCount) {
            lastPageReached = true;
        } else {
            pageOffset += items.size();
        }
        removeLoadingBar();

        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void onDataFailed() {
        removeLoadingBar();
        notifyDataSetChanged();
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public void setPageLoader(PageLoader pageLoader) {
        this.pageLoader = pageLoader;
    }

    public PaginationRvScrollListener getListener() {
        return listener;
    }

    public int getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset) {
        this.pageOffset = pageOffset;
    }

    private class FooterViewHolder extends BaseViewHolder<PaginationRvFooterBinding, T> {

        FooterViewHolder(PaginationRvFooterBinding dataBinding) {
            super(dataBinding);
        }

        @Override
        public void bind(T item) {
            binding.paginationRvFooterPb.getIndeterminateDrawable()
                    .setColorFilter(ContextCompat.getColor(itemView.getContext(), R.color.primary),
                            PorterDuff.Mode.SRC_IN);
        }
    }

    private void showLoadingBar() {
        if (!hasFooter() && needFooter()) {
            items.add(new PaginationRvFooter());
            notifyItemInserted(items.size() - 1);
            pageLoader.loadData(pageOffset);
        }
    }

    private void removeLoadingBar() {
        if (hasFooter()) {
            int lastIndex = items.size() - 1;
            items.remove(lastIndex);
            notifyItemRemoved(lastIndex);
        }
    }

    private boolean hasFooter() {
        if (items != null && items.size() > 0) {
            return items.get(items.size() - 1) instanceof PaginationRvFooter;
        }
        return false;
    }

    private boolean needFooter() {
        return items != null && items.size() > 0;
    }

    private static class PaginationRvFooter implements PaginationRvItem {
    }

    public interface PageLoader {
        void loadData(int offset);
    }
}
