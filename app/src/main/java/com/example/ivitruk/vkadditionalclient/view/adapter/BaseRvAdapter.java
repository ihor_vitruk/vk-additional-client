package com.example.ivitruk.vkadditionalclient.view.adapter;

import android.support.v7.widget.RecyclerView;

import com.example.ivitruk.vkadditionalclient.di.component.ViewModelComponent;
import com.example.ivitruk.vkadditionalclient.view.viewholder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRvAdapter<T, VH extends BaseViewHolder> extends RecyclerView.Adapter<VH> {

    protected final List<T> items = new ArrayList<>();

    protected ViewModelComponent viewModelComponent;

    public BaseRvAdapter(ViewModelComponent viewModelComponent) {
        this.viewModelComponent = viewModelComponent;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<T> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public T getItem(int position) {
        if (items.size() == 0 || position < 0 || position >= items.size()) {
            return null;
        }
        return items.get(position);
    }
}
