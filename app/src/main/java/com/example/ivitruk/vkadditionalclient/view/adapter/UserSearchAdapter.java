package com.example.ivitruk.vkadditionalclient.view.adapter;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.UserItemBinding;
import com.example.ivitruk.vkadditionalclient.di.component.ViewModelComponent;
import com.example.ivitruk.vkadditionalclient.model.api.response.User;
import com.example.ivitruk.vkadditionalclient.view.viewholder.UserItemViewHolder;

public class UserSearchAdapter extends BaseRvAdapter<User, UserItemViewHolder> {

    public UserSearchAdapter(ViewModelComponent viewModelComponent) {
        super(viewModelComponent);
    }

    @Override
    public UserItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UserItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.user_item, parent, false);
        return new UserItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(UserItemViewHolder holder, int position) {
        holder.bind(items.get(position));
    }
}
