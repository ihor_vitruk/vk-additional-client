package com.example.ivitruk.vkadditionalclient.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.FollowersFragmentBinding;
import com.example.ivitruk.vkadditionalclient.view.adapter.UsersAdapter;
import com.example.ivitruk.vkadditionalclient.viewmodel.FollowersViewModel;

import javax.inject.Inject;

public class FollowersFragment extends BaseFragment<FollowersViewModel, FollowersFragmentBinding> {

    @Inject
    FollowersViewModel followersViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        viewModel.setupRecyclerView(binding.followersRv, new UsersAdapter(viewModelComponent));
        return view;
    }

    @NonNull
    @Override
    public FollowersViewModel initViewModel() {
        viewModelComponent.inject(this);
        return followersViewModel;
    }

    @NonNull
    @Override
    public FollowersFragmentBinding initBinding(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FollowersFragmentBinding binding = DataBindingUtil.inflate(
                (LayoutInflater.from(getContext())),
                R.layout.followers_fragment, container, false);
        binding.setViewModel(viewModel);
        return binding;
    }
}
