package com.example.ivitruk.vkadditionalclient.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.MessagesFragmentBinding;
import com.example.ivitruk.vkadditionalclient.view.adapter.MessagesAdapter;
import com.example.ivitruk.vkadditionalclient.viewmodel.MessagesViewModel;

import javax.inject.Inject;

public class MessagesFragment extends BaseFragment<MessagesViewModel, MessagesFragmentBinding> {

    @Inject
    MessagesViewModel messagesViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        viewModel.setupRecyclerView(binding.messagesRv, new MessagesAdapter(viewModelComponent));
        return view;
    }

    @NonNull
    @Override
    public MessagesViewModel initViewModel() {
        viewModelComponent.inject(this);
        return messagesViewModel;
    }

    @NonNull
    @Override
    public MessagesFragmentBinding initBinding(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MessagesFragmentBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(getContext()),
                        R.layout.messages_fragment, container, false);
        binding.setViewModel(viewModel);
        return binding;
    }
}
