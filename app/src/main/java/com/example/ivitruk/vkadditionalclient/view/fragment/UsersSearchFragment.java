package com.example.ivitruk.vkadditionalclient.view.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.databinding.UserSearchFragmentBinding;
import com.example.ivitruk.vkadditionalclient.util.UiUtil;
import com.example.ivitruk.vkadditionalclient.view.adapter.UserSearchAdapter;
import com.example.ivitruk.vkadditionalclient.viewmodel.UsersSearchViewModel;

import javax.inject.Inject;

public class UsersSearchFragment extends BaseFragment<UsersSearchViewModel, UserSearchFragmentBinding> {

    @Inject
    UsersSearchViewModel usersSearchViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        viewModel.setupRecyclerView(binding.usersRv, new UserSearchAdapter(viewModelComponent));
        viewModel.setupExpandableLayout(binding.userSearchParametersContent, getChildFragmentManager());

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_global, menu);
        super.onCreateOptionsMenu(menu, inflater);

        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                viewModel.setSearchQuery(newText);
                viewModel.refreshData();
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_like:
                Context context = getContext();
                int itemCount = usersSearchViewModel.userSearchAdapter.getItemCount();
                UiUtil.newConfirmationDialog(context,
                        String.format(context.getString(R.string.like_users_dialog_title), itemCount),
                        String.format(context.getString(R.string.like_users_dialog_message), itemCount),
                        context.getString(R.string.ok),
                        context.getString(R.string.cancel),
                        (dialog, which) -> viewModel.startLikingProcess()
                ).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @NonNull
    @Override
    public UsersSearchViewModel initViewModel() {
        viewModelComponent.inject(this);
        return usersSearchViewModel;
    }

    @NonNull
    @Override
    public UserSearchFragmentBinding initBinding(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        UserSearchFragmentBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(getContext()),
                        R.layout.user_search_fragment, container, false);
        binding.setViewModel(viewModel);
        return binding;
    }

    public UserSearchFragmentBinding getBinding() {
        return binding;
    }
}
