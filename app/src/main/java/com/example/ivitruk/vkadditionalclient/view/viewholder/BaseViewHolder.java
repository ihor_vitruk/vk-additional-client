package com.example.ivitruk.vkadditionalclient.view.viewholder;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import com.example.ivitruk.vkadditionalclient.App;
import com.example.ivitruk.vkadditionalclient.di.component.ViewModelComponent;
import com.example.ivitruk.vkadditionalclient.di.module.ApiModule;
import com.example.ivitruk.vkadditionalclient.di.module.BusinessModule;
import com.example.ivitruk.vkadditionalclient.di.module.ViewModelModule;

public abstract class BaseViewHolder<DB extends ViewDataBinding, T> extends RecyclerView.ViewHolder {

    protected DB binding;

    protected ViewModelComponent viewModelComponent;

    public BaseViewHolder(DB dataBinding) {
        super(dataBinding.getRoot());
        viewModelComponent = App.getAppComponent()
                .plus(new ApiModule())
                .plus(new BusinessModule())
                .plus(new ViewModelModule(itemView.getContext()));
        this.binding = dataBinding;
    }

    public abstract void bind(T item);
}
