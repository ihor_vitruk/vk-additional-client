package com.example.ivitruk.vkadditionalclient.view.viewholder;

import com.example.ivitruk.vkadditionalclient.DrawerItem;
import com.example.ivitruk.vkadditionalclient.databinding.DrawerItemBinding;
import com.example.ivitruk.vkadditionalclient.viewmodel.DrawerItemViewModel;

import javax.inject.Inject;

public class DrawerViewHolder extends BaseViewHolder<DrawerItemBinding, DrawerItem> {

    @Inject
    DrawerItemViewModel drawerItemViewModel;

    public DrawerViewHolder(DrawerItemBinding dataBinding) {
        super(dataBinding);
        viewModelComponent.inject(this);
    }

    @Override
    public void bind(DrawerItem item) {
        if (binding.getViewModel() == null) {
            drawerItemViewModel.setDrawerItem(item);
            binding.setViewModel(drawerItemViewModel);
        } else {
            binding.getViewModel().setDrawerItem(item);
        }
    }
}
