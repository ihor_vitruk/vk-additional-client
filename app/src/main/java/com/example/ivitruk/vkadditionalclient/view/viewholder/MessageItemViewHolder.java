package com.example.ivitruk.vkadditionalclient.view.viewholder;

import com.example.ivitruk.vkadditionalclient.model.api.response.Message;
import com.example.ivitruk.vkadditionalclient.databinding.MessageItemBinding;
import com.example.ivitruk.vkadditionalclient.viewmodel.MessageItemViewModel;

import javax.inject.Inject;

public class MessageItemViewHolder extends BaseViewHolder<MessageItemBinding, Message> {

    @Inject
    MessageItemViewModel messageItemViewModel;

    public MessageItemViewHolder(MessageItemBinding dataBinding) {
        super(dataBinding);
        viewModelComponent.inject(this);
    }

    @Override
    public void bind(Message item) {
        if (binding.getViewModel() == null) {
            messageItemViewModel.setMessage(item);
            binding.setViewModel(messageItemViewModel);
        } else {
            binding.getViewModel().setMessage(item);
        }
    }
}
