package com.example.ivitruk.vkadditionalclient.view.viewholder;

import com.example.ivitruk.vkadditionalclient.model.api.response.User;
import com.example.ivitruk.vkadditionalclient.databinding.UserItemBinding;
import com.example.ivitruk.vkadditionalclient.viewmodel.UserItemViewModel;

import javax.inject.Inject;

public class UserItemViewHolder extends BaseViewHolder<UserItemBinding, User> {

    @Inject
    UserItemViewModel userItemViewModel;

    public UserItemViewHolder(UserItemBinding dataBinding) {
        super(dataBinding);
        viewModelComponent.inject(this);
    }

    @Override
    public void bind(User item) {
        if (binding.getViewModel() == null) {
            userItemViewModel.setUser(item);
            binding.setViewModel(userItemViewModel);
        } else {
            binding.getViewModel().setUser(item);
        }
    }
}
