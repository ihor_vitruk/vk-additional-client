package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;

import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.manager.data.Session;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.AuthDataListener;
import com.example.ivitruk.vkadditionalclient.view.activity.SplashActivity;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseViewModel extends BaseObservable {

    protected IAuthBusinessLayer authBusinessLayer;

    protected ISessionManager sessionManager;

    protected IInfoManager infoManager;

    protected IActivityNavigationManager activityNavigationManager;

    protected ConnectivityReceiver connectivityReceiver;

    private final List<IBusinessLayer> businessLayers = new ArrayList<>();

    protected Context context;

    public BaseViewModel(Context context, IAuthBusinessLayer authBusinessLayer,
                         ConnectivityReceiver connectivityReceiver,
                         ISessionManager sessionManager,
                         IInfoManager infoManager,
                         IActivityNavigationManager activityNavigationManager) {
        this.context = context;
        this.authBusinessLayer = authBusinessLayer;
        this.infoManager = infoManager;
        this.sessionManager = sessionManager;
        this.activityNavigationManager = activityNavigationManager;
        this.connectivityReceiver = connectivityReceiver;
        authBusinessLayer.setDataListener(authDataListener);
    }

    protected void initBusinessLayers() {
    }

    protected final <T extends IBusinessLayer> void addBusinessLayer(T businessLayer) {
        businessLayers.add(businessLayer);
    }

    public void create() {
        initBusinessLayers();
        connectivityReceiver.register(this::refreshData, context);
    }

    public void destroy() {
        infoManager.hideProgress();
        connectivityReceiver.unregister(context);
        for (IBusinessLayer businessLayer : businessLayers) {
            businessLayer.destroy();
        }
        businessLayers.clear();
    }

    public void refreshData() {
        infoManager.hideMessage();
    }

    private AuthDataListener authDataListener = new AuthDataListener() {

        @Override
        public void onLoginSuccess(Session session) {
            super.onLoginSuccess(session);
            sessionManager.saveSession(session);
            activityNavigationManager.startSingle(SplashActivity.class);
        }

        @Override
        public void onLoginError(Throwable t) {
            super.onLoginError(t);
            infoManager.showErrorMessage(t, null);
        }

        @Override
        public void onLogoutSuccess() {
            super.onLogoutSuccess();
            infoManager.hideProgress();
            sessionManager.deleteSession();
            activityNavigationManager.startSingle(SplashActivity.class);
        }

        @Override
        public void onLogoutError(Throwable t) {
            super.onLogoutError(t);
            infoManager.hideProgress();
            sessionManager.deleteSession();
            activityNavigationManager.startSingle(SplashActivity.class);
        }
    };
}
