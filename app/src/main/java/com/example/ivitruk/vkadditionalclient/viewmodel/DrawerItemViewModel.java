package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;

import com.example.ivitruk.vkadditionalclient.DrawerItem;
import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

public class DrawerItemViewModel extends BaseViewModel {

    private DrawerItem drawerItem;

    public DrawerItemViewModel(Context context, IAuthBusinessLayer authBusinessLayer, ConnectivityReceiver connectivityReceiver, ISessionManager sessionManager, IInfoManager infoManager, IActivityNavigationManager activityNavigationManager) {
        super(context, authBusinessLayer, connectivityReceiver, sessionManager, infoManager, activityNavigationManager);
    }
    
    public DrawerItem getDrawerItem() {
        return drawerItem;
    }

    public void setDrawerItem(DrawerItem drawerItem) {
        this.drawerItem = drawerItem;
    }
}
