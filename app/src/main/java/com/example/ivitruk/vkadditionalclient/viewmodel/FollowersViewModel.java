package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.api.response.User;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IUsersBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.UsersDataListener;
import com.example.ivitruk.vkadditionalclient.view.adapter.UsersAdapter;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

import java.util.List;

public class FollowersViewModel extends BaseViewModel {

    private UsersAdapter usersAdapter;

    private IUsersBusinessLayer usersBusinessLayer;

    public FollowersViewModel(Context context, IAuthBusinessLayer authBusinessLayer,
                              ConnectivityReceiver connectivityReceiver,
                              ISessionManager sessionManager, IInfoManager infoManager,
                              IActivityNavigationManager activityNavigationManager,
                              IUsersBusinessLayer usersBusinessLayer) {
        super(context, authBusinessLayer, connectivityReceiver, sessionManager, infoManager, activityNavigationManager);
        this.usersBusinessLayer = usersBusinessLayer;
    }

    @Override
    protected void initBusinessLayers() {
        super.initBusinessLayers();
        usersBusinessLayer.setDataListener(usersDataListener);
        addBusinessLayer(usersBusinessLayer);
    }

    @Override
    public void refreshData() {
        super.refreshData();
        retrieveFollowers(true, true);
    }

    public void setupRecyclerView(RecyclerView recyclerView, UsersAdapter usersAdapter) {
        this.usersAdapter = usersAdapter;
        usersAdapter.setPageLoader(offset -> retrieveFollowers(false, false));
        recyclerView.setAdapter(usersAdapter);
        recyclerView.addOnScrollListener(usersAdapter.getListener());
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }

    private void retrieveFollowers(boolean resetData, boolean showProgress) {
        if (resetData) {
            usersAdapter.resetData();
        }
        if (showProgress) {
            infoManager.showProgress();
        }
        usersBusinessLayer.getFollowers(usersAdapter.getPageCount(), usersAdapter.getPageOffset());
    }

    private UsersDataListener usersDataListener = new UsersDataListener(authBusinessLayer) {
        @Override
        public void onGetFollowersSuccess(List<User> users) {
            super.onGetFollowersSuccess(users);
            infoManager.hideProgress();
            usersAdapter.onDataReceived(users);
        }

        @Override
        public void onGetFollowersError(Throwable t) {
            super.onSearchUsersError(t);
            infoManager.hideProgress();
            usersAdapter.onDataFailed();
            infoManager.showErrorMessage(t, FollowersViewModel.this);
        }
    };
}
