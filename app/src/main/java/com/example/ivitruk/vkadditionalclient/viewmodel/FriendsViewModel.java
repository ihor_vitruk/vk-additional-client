package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.api.response.User;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IFriendsBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.FriendsDataListener;
import com.example.ivitruk.vkadditionalclient.view.adapter.UsersAdapter;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

import java.util.List;

public class FriendsViewModel extends BaseViewModel {

    private UsersAdapter usersAdapter;

    private IFriendsBusinessLayer friendsBusinessLayer;

    private String searchQuery;

    public FriendsViewModel(Context context, IAuthBusinessLayer authBusinessLayer,
                            ConnectivityReceiver connectivityReceiver,
                            ISessionManager sessionManager, IInfoManager infoManager,
                            IActivityNavigationManager activityNavigationManager,
                            IFriendsBusinessLayer friendsBusinessLayer) {
        super(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
        this.friendsBusinessLayer = friendsBusinessLayer;
    }

    @Override
    protected void initBusinessLayers() {
        super.initBusinessLayers();
        friendsBusinessLayer.setDataListener(friendsDataListener);
        addBusinessLayer(friendsBusinessLayer);
    }

    @Override
    public void refreshData() {
        super.refreshData();
        retrieveFriends(true, true);
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public void setupRecyclerView(RecyclerView rv, UsersAdapter usersAdapter) {
        this.usersAdapter = usersAdapter;
        usersAdapter.setPageLoader(offset -> retrieveFriends(false, false));
        rv.setAdapter(usersAdapter);
        rv.addOnScrollListener(usersAdapter.getListener());
        rv.setLayoutManager(new LinearLayoutManager(context));
    }

    private void retrieveFriends(boolean resetData, boolean showProgress) {
        if (resetData) {
            usersAdapter.resetData();
        }
        if (showProgress) {
            infoManager .showProgress();
        }
        friendsBusinessLayer.searchFriends(searchQuery, usersAdapter.getPageCount(), usersAdapter.getPageOffset());
    }

    private FriendsDataListener friendsDataListener = new FriendsDataListener(authBusinessLayer) {
        @Override
        public void onGetFriendsSuccess(List<User> users) {
            super.onGetFriendsSuccess(users);
            infoManager.hideProgress();
            usersAdapter.onDataReceived(users);
        }

        @Override
        public void onGetFriendsError(Throwable t) {
            super.onGetFriendsError(t);
            infoManager.hideProgress();
            usersAdapter.onDataFailed();
            infoManager.showErrorMessage(t, FriendsViewModel.this);
        }
    };
}
