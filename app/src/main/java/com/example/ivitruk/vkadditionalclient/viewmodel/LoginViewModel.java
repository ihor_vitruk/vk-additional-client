package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;
import android.webkit.WebView;

import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.view.activity.MainActivity;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

public class LoginViewModel extends BaseViewModel {

    public LoginViewModel(Context context, IAuthBusinessLayer authBusinessLayer,
                          ConnectivityReceiver connectivityReceiver,
                          ISessionManager sessionManager,
                          IInfoManager infoManager,
                          IActivityNavigationManager activityNavigationManager) {
        super(context, authBusinessLayer, connectivityReceiver, sessionManager,
                infoManager, activityNavigationManager);
    }

    public void prepareForLogin(WebView webView) {
        infoManager.showProgress();
        authBusinessLayer.login(webView);
    }
}
