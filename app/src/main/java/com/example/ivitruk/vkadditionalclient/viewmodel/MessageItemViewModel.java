package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;

import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.api.response.Message;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

public class MessageItemViewModel extends BaseViewModel {

    private Message message;

    public MessageItemViewModel(Context context, IAuthBusinessLayer authBusinessLayer,
                                ConnectivityReceiver connectivityReceiver,
                                ISessionManager sessionManager,
                                IInfoManager infoManager,
                                IActivityNavigationManager activityNavigationManager) {
        super(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
    }

    public void setMessage(Message message) {
        this.message = message;
        notifyChange();
    }

    public Message getMessage() {
        return message;
    }
}
