package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.api.response.Message;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IMessagesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.MessagesDataListener;
import com.example.ivitruk.vkadditionalclient.view.adapter.MessagesAdapter;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

import java.util.List;

public class MessagesViewModel extends BaseViewModel {

    public IMessagesBusinessLayer messagesBusinessLayer;

    private MessagesAdapter messagesAdapter;

    public MessagesViewModel(Context context,
                             IAuthBusinessLayer authBusinessLayer,
                             ConnectivityReceiver connectivityReceiver,
                             ISessionManager sessionManager,
                             IInfoManager infoManager,
                             IActivityNavigationManager activityNavigationManager,
                             IMessagesBusinessLayer messagesBusinessLayer) {
        super(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
        this.messagesBusinessLayer = messagesBusinessLayer;
    }

    @Override
    protected void initBusinessLayers() {
        super.initBusinessLayers();
        messagesBusinessLayer.setDataListener(messagesDataListener);
        addBusinessLayer(messagesBusinessLayer);
    }

    @Override
    public void refreshData() {
        super.refreshData();
        retrieveHistory();
    }

    public void setupRecyclerView(RecyclerView rv, MessagesAdapter messagesAdapter) {
        this.messagesAdapter = messagesAdapter;
        rv.setAdapter(messagesAdapter);
        rv.setLayoutManager(new LinearLayoutManager(context));
    }

    private void retrieveHistory() {
        infoManager.showProgress();
        messagesBusinessLayer.getHistory("134809073", 50);
    }

    private MessagesDataListener messagesDataListener = new MessagesDataListener(authBusinessLayer) {
        @Override
        public void onGetHistorySuccess(List<Message> messages) {
            super.onGetHistorySuccess(messages);
            infoManager.hideProgress();
            messagesAdapter.setItems(messages);
        }

        @Override
        public void onGetHistoryError(Throwable t) {
            super.onGetHistoryError(t);
            infoManager.hideProgress();
            infoManager.showErrorMessage(t, MessagesViewModel.this);
        }
    };
}
