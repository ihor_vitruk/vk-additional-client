package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;

import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.view.activity.LoginActivity;
import com.example.ivitruk.vkadditionalclient.view.activity.MainActivity;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;

public class SplashViewModel extends BaseViewModel {

    public SplashViewModel(Context context,
                           IAuthBusinessLayer authBusinessLayer,
                           ConnectivityReceiver connectivityReceiver,
                           ISessionManager sessionManager,
                           IInfoManager infoManager,
                           IActivityNavigationManager activityNavigationManager) {
        super(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
    }

    public void processAuthorization() {
        if (authBusinessLayer.isLoggedIn()) {
            activityNavigationManager.startSingle(MainActivity.class);
        } else {
            activityNavigationManager.startSingle(LoginActivity.class);
        }
    }
}
