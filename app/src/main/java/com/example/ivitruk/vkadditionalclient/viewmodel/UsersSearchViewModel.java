package com.example.ivitruk.vkadditionalclient.viewmodel;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableInt;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.example.ivitruk.vkadditionalclient.R;
import com.example.ivitruk.vkadditionalclient.UserSearchSettings;
import com.example.ivitruk.vkadditionalclient.databinding.UserSearchFragmentBinding;
import com.example.ivitruk.vkadditionalclient.manager.IActivityNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IFragmentNavigationManager;
import com.example.ivitruk.vkadditionalclient.manager.IInfoManager;
import com.example.ivitruk.vkadditionalclient.manager.ISessionManager;
import com.example.ivitruk.vkadditionalclient.model.api.ApiData;
import com.example.ivitruk.vkadditionalclient.model.api.response.User;
import com.example.ivitruk.vkadditionalclient.model.business.IAuthBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.ILikesBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.business.IUsersBusinessLayer;
import com.example.ivitruk.vkadditionalclient.model.datalistener.LikesDataListener;
import com.example.ivitruk.vkadditionalclient.model.datalistener.UsersDataListener;
import com.example.ivitruk.vkadditionalclient.util.UiUtil;
import com.example.ivitruk.vkadditionalclient.view.adapter.UserSearchAdapter;
import com.example.ivitruk.vkadditionalclient.view.fragment.UsersSearchParametersFragment;
import com.example.ivitruk.vkadditionalclient.view.receiver.ConnectivityReceiver;
import com.github.aakira.expandablelayout.ExpandableLayoutListener;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.util.List;
import java.util.Random;

public class UsersSearchViewModel extends BaseViewModel {

    public IUsersBusinessLayer usersBusinessLayer;

    public ILikesBusinessLayer likesBusinessLayer;

    public ObservableInt usersCount = new ObservableInt();

    public UserSearchAdapter userSearchAdapter;

    private IFragmentNavigationManager fragmentNavigationManager;

    private UserSearchSettings userSearchSettings;

    private String searchQuery;

    public UsersSearchViewModel(Context context,
                                IAuthBusinessLayer authBusinessLayer,
                                ConnectivityReceiver connectivityReceiver,
                                ISessionManager sessionManager,
                                IInfoManager infoManager,
                                IActivityNavigationManager activityNavigationManager,
                                IUsersBusinessLayer usersBusinessLayer,
                                ILikesBusinessLayer likesBusinessLayer,
                                IFragmentNavigationManager fragmentNavigationManager) {
        super(context, authBusinessLayer, connectivityReceiver,
                sessionManager, infoManager, activityNavigationManager);
        this.usersBusinessLayer = usersBusinessLayer;
        this.likesBusinessLayer = likesBusinessLayer;
        this.fragmentNavigationManager = fragmentNavigationManager;
        userSearchSettings = UserSearchSettings.read(context);
    }


    @Override
    protected void initBusinessLayers() {
        super.initBusinessLayers();
        usersBusinessLayer.setDataListener(usersDataListener);
        addBusinessLayer(usersBusinessLayer);

        likesBusinessLayer.setDataListener(likesDataListener);
        addBusinessLayer(likesBusinessLayer);
    }

    @Override
    public void refreshData() {
        super.refreshData();
        searchUsers(searchQuery, true);
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public void setupRecyclerView(RecyclerView rv, UserSearchAdapter userSearchAdapter) {
        this.userSearchAdapter = userSearchAdapter;
        rv.setAdapter(userSearchAdapter);
        rv.setLayoutManager(new LinearLayoutManager(context));
    }

    public void setupExpandableLayout(ExpandableRelativeLayout el,
                                      FragmentManager fragmentManager) {
        el.setListener(new ExpandableLayoutListener() {
            @Override
            public void onAnimationStart() {

            }

            @Override
            public void onAnimationEnd() {

            }

            @Override
            public void onPreOpen() {

            }

            @Override
            public void onPreClose() {

            }

            @Override
            public void onOpened() {

            }

            @Override
            public void onClosed() {
                UserSearchSettings newUserSearchSettings = UserSearchSettings.read(context);
                if (!userSearchSettings.equals(newUserSearchSettings)) {
                    userSearchSettings = newUserSearchSettings;
                    refreshData();
                }
            }
        });

        fragmentNavigationManager.replaceFragment(R.id.user_search_parameters_fragment_container,
                UsersSearchParametersFragment.class, fragmentManager);
    }

    private void searchUsers(String searchText, boolean showProgress) {
        if (showProgress) {
            infoManager.showProgress();
        }

        usersBusinessLayer.search(searchText, ApiData.DEFAULT_FIELDS,
                1057, 2, userSearchSettings.getSort(),
                userSearchSettings.getStatus(), userSearchSettings.getSex(),
                userSearchSettings.getAgeFrom(), userSearchSettings.getAgeTo(),
                userSearchSettings.getBirthDay(), userSearchSettings.getBirthMonth(),
                userSearchSettings.getOnline(), userSearchSettings.getHasPhoto(),
                userSearchSettings.getCanWritePrivateMessage(), userSearchSettings.getCanSendFriendRequest(),
                userSearchSettings.getBlacklisted(), userSearchSettings.getBlacklistedByMe(),
                userSearchSettings.getFriendStatus());
    }

    private UsersDataListener usersDataListener = new UsersDataListener(authBusinessLayer) {

        @Override
        public void onSearchUsersSuccess(List<User> users) {
            super.onSearchUsersSuccess(users);
            infoManager.hideProgress();
            userSearchAdapter.setItems(users);
            usersCount.set(userSearchAdapter.getItemCount());
        }

        @Override
        public void onSearchUsersError(Throwable t) {
            super.onSearchUsersError(t);
            infoManager.hideProgress();
            infoManager.showErrorMessage(t, UsersSearchViewModel.this);
        }
    };

    private LikesDataListener likesDataListener = new LikesDataListener(authBusinessLayer) {
        @Override
        public void onAddLikeSuccess() {
            super.onAddLikeSuccess();
            successCount++;
            like(1000);
        }

        @Override
        public void onAddLikeError(Throwable t) {
            super.onAddLikeError(t);
            onLikeError(10000);
        }
    };

    public void expandSearchParameters(View v) {
        UserSearchFragmentBinding binding = DataBindingUtil.findBinding(v);
        boolean isExpanded = binding.userSearchParametersContent.isExpanded();
        binding.userSearchParametersContent.toggle();
        binding.userSearchParametersHeaderArrow.animate().rotation(isExpanded ? 0 : 180).start();
    }

    //update likes cycle

    public void onLikeError(int pauseTime) {
        errorCount++;
        like(pauseTime);
    }

    private int currentLikeItemPosition = 0;
    private int successCount = 0;
    private int errorCount = 0;

    public void startLikingProcess() {
        if (likingDialog != null && likingDialog.isShowing()) {
            likingDialog.dismiss();
            likingDialog = null;
        }

        int itemCount = userSearchAdapter.getItemCount();
        likingDialog = UiUtil.newMessageDialog(context,
                String.format(context.getString(R.string.like_users_dialog_title), itemCount),
                String.format(context.getString(R.string.like_users_dialog_message), itemCount),
                context.getString(R.string.cancel),
                (dialog, which) -> isLiking = false).create();
        isLiking = true;
        likingDialog.show();
        like(0);
    }

    private AlertDialog likingDialog;

    private boolean isLiking;

    private void like(int timeBeforeNext) {
        likingDialog.setMessage("Success: " + successCount + "\n" + "Error: " + errorCount);

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            if (currentLikeItemPosition < userSearchAdapter.getItemCount() && isLiking) {
                User user = userSearchAdapter.getItem(currentLikeItemPosition);
                currentLikeItemPosition++;
                likingDialog.setTitle(String.format(context.getString(R.string.like_users_dialog_liking_user), user.getFirstName() + " " + user.getLastName()));
                String photoId = user.getPhotoId();
                if (TextUtils.isEmpty(photoId)) {
                    onLikeError(1000);
                    return;
                }
                String[] parts = photoId.split("_");
                int itemId = Integer.valueOf(parts[1]);
                likesBusinessLayer.add("photo", user.getId(), itemId);
            } else {
                likingDialog.setTitle("Finished");
                currentLikeItemPosition = 0;
                successCount = 0;
                errorCount = 0;
                isLiking = false;
            }
        }, timeBeforeNext);
    }
}
